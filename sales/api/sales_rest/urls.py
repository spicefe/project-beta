from django.urls import path

from .views import list_customers, show_customer, list_sales, show_sale, list_salespeople, show_salespeople

urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("salespeople/<int:pk>/", show_salespeople, name="show_salespeople"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:pk>/", show_customer, name="show_customer"),
    path("sales/", list_sales, name="create_sales"),
    path("automobiles/<int:automobile_vo_id>/sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", show_sale, name="show_sale"),
]
