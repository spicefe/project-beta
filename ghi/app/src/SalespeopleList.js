import React, {useState, useEffect} from 'react'

async function fetchData() {
    const salespeopleFetch = await fetch('http://localhost:8090/api/salespeople/');
    if (salespeopleFetch.ok) {
        const salespeopleData = await salespeopleFetch.json();
        return salespeopleData;
    } else {
        console.error('Error fetching salespeople data');
    }
}

function SalespeopleList() {
    const[salespeopleData, setSalespeopleData] = useState([]);

    useEffect(()=>{
        fetchData().then(data => {
            setSalespeopleData(data.salespeople);
        })
    }, []);

    async function deleteSalesperson(id) {
        const salespersonURL = `http://localhost:8090/api/salespeople/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(salespersonURL, fetchConfig);
        if (response.ok) {
            window.location.reload(false);
        }
    }

    return (
        <div>
            <h1 className="text-center">Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeopleData.map(salesperson => {
                    return (
                        <tr key={salesperson.id}>
                            <td>{ salesperson.employee_id }</td>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                            <td><button className="btn btn-danger" onClick={() => {deleteSalesperson(salesperson.id)}}>Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SalespeopleList;
