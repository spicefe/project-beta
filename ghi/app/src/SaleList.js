import React, {useState, useEffect} from 'react'

async function fetchData() {
    const salesFetch = await fetch('http://localhost:8090/api/sales/');
    if (salesFetch.ok) {
        const salesData = await salesFetch.json();
        return salesData;
    } else {
        console.error('Error fetching sale data');
    }
}

function SaleList() {
    const[saleData, setSaleData] = useState([]);

    useEffect(()=>{
        fetchData().then(data => {
            setSaleData(data.sales);
        })
    }, []);

    async function deleteSale(id) {
        const saleURL = `http://localhost:8090/api/sales/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(saleURL, fetchConfig);
        if (response.ok) {
            window.location.reload(false);
        }
    }

    return (
        <div>
            <h1 className="text-center">Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {saleData.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{ sale.salesperson_employee_id }</td>
                            <td>{ sale.salesperson_first_name } { sale.salesperson_last_name }</td>
                            <td>{ sale.customer_first_name } { sale.customer_last_name }</td>
                            <td>{ sale.vin }</td>
                            <td>{ sale.price }</td>
                            <td><button className="btn btn-danger" onClick={() => {deleteSale(sale.id)}}>Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SaleList;
