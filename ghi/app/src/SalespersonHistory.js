import React, {useState, useEffect} from 'react'


function SalespersonHistory() {
    const[saleData, setSaleData] = useState([]);
    const[salespeopleData, setSalespeopleData] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState('');

    async function fetchData() {
        const salesFetch = await fetch('http://localhost:8090/api/sales/');
        const salespeopleFetch = await fetch('http://localhost:8090/api/salespeople/');
        if (salesFetch.ok && salespeopleFetch.ok) {
            const salesData = await salesFetch.json();
            const salespeopleData = await salespeopleFetch.json();
            setSaleData(salesData.sales);
            setSalespeopleData(salespeopleData.salespeople);
        } else {
            console.error('Error fetching data');
        }
    }

    useEffect(()=>{
        fetchData();
    }, []);

    return (
        <div>
            <h1 className="text-center">Salesperson History</h1>
            <select value={selectedSalesperson} onChange={(event) => setSelectedSalesperson(event.target.value)}>
            <option value="">-- Select a Salesperson --</option>
            {salespeopleData.map(salesperson => (
                <option key={salesperson.id} value={salesperson.id}>
                    {salesperson.first_name} {salesperson.last_name}
                </option>
            ))}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {saleData
                        .filter((sale) => sale.salesperson_id === Number(selectedSalesperson))
                        .map((sale) => (
                            <tr key={sale.id}>
                                <td>
                                    {sale.salesperson_first_name} {sale.salesperson_last_name}
                                </td>
                                <td>
                                    {sale.customer_first_name} {sale.customer_last_name}
                                </td>
                                <td>{sale.vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalespersonHistory;
