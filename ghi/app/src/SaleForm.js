import React, { useEffect, useState, } from 'react';

function SaleForm() {
    const [autos, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }
    const [automobile, setAutomobile] = useState('');
    const handleAutomobileChange = (event) => {
        const value = event.target.value
        setAutomobile(value);
    }
    const [salesperson, setSalesperson] = useState('');
    const handleSalespersonChange = (event) => {
        const value = event.target.value
        setSalesperson(value);
    }
    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.automobile = automobile;
        data.customer = customer;
        data.salesperson = salesperson;
        data.price = price;


        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();

            const automobileUrl = `http://localhost:8100${automobile}`;
            const automobileConfig = {
                method: 'PUT',
                body: JSON.stringify({ ...automobile, sold: true}),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const automobileResponse = await fetch(automobileUrl, automobileConfig);
            if (automobileResponse.ok) {
                console.log('Automobile successfully sold');
            } else {
                console.error('Failed to sell automobile');
            }

            setPrice('');
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
        }
    }
    const fetchData = async () => {
        const automobilesFetch = await fetch('http://localhost:8100/api/automobiles/');
        const customersFetch = await fetch('http://localhost:8090/api/customers/');
        const salespeopleFetch = await fetch('http://localhost:8090/api/salespeople/');

        if (automobilesFetch.ok && customersFetch.ok && salespeopleFetch.ok) {
            const automobileData = await automobilesFetch.json();
            const unsoldAutomobiles = automobileData.autos.filter(
                automobile => automobile.sold === false
            );
            const customerData = await customersFetch.json();
            const salespeopleData = await salespeopleFetch.json();
            setAutomobiles(unsoldAutomobiles);
            setCustomers(customerData.customers);
            setSalespeople(salespeopleData.salespeople)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
              <div className="mb-3">
                <select value={automobile} onChange={handleAutomobileChange} required id="automobile" name="automobile" className="form-select">
                  <option value="">Choose an automobile VIN</option>
                  {autos.map(automobile => {
                    return (
                        <option key={automobile.href} value={automobile.href}>
                            {automobile.vin}
                        </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
                  <option value="">Choose a salesperson</option>
                  {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select value={customer} onChange={handleCustomerChange} required id="customer" name="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {customers.map(customer => {
                    return (
                        <option key={customer.id} value={customer.id}>
                            {customer.first_name} {customer.last_name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input value={price} onChange={handlePriceChange} placeholder="Price" required type="number" name="price" id="price" className="form-control"/>
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default SaleForm;
