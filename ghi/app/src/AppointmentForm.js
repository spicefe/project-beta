import React, {useEffect,useState} from "react";

function AppointmentForm(){
    const [autos, setAutomobiles] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [date, setDates] = useState([]);
    const [time, setTime] = useState([]);
    const [technician, setTechnicians]= useState([]);
    const [reason, setReasons] = useState([]);

    const [automobile, setAutomobile] = useState('');
    const handleAutomobileChange = (event) => {
        const value = event.target.value
        setAutomobile(value);
    }

    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value);
    }

    const [dates, setDate] = useState('');
    const handleDatesChange = (event) =>{
        setDates(value);
    }

    const [newtime,setnewTime] = useState('');
    const handleTimeChange = (event) =>{
        setTime(value);
    }

    const handleSubmit = async(event) =>{
        event.preventDefault();

        const data={};
        data.automobile = automobile;
        data.customer = customer;
        data.dates = dates;
        data.newtime = newtime;


        const finAppointmentUrl= 'http://localhost:8080/api/appointments';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers:{
                'Contenet-Type': 'application/json',
            }
        };
        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            const appointmentUrl = `http://localhost:8080${appointments}`;
            const appointmentConfig ={
                method:'PUT',
                body:JSON.stringify({... appointment, complete:true}),
                headers: {
                    'content-Type': 'application/json',
                }
            }
            const appointmentResponse = await fetch(appointmentUrl, appointmentConfig);
            if (appointmentResponse.ok){
                console.log('Appointment successfully booked');
            }  else {
                console.error('Failed to book appointment');
            }
            setDate('');
            setTime('');
            setTechnicians('');
            setReasons('');
        }
    }
    const fetchData = async () => {
        const automobilesFetch = await fetch('http://localhost:8100/api/automobiles/');
        const customersFetch = await fetch('http://localhost:8090/api/customers/');
        const technicianfetch = await fetch('http://localhost:8080/api/technicians/');

        if (automobilesFetch.ok && customersFetch.ok && technicianfetch.ok) {
            const automobileData = await automobilesFetch.json();
            const customerData = await customersFetch.json();
            const technicianData = await technicianfetch.json();
            setAutomobiles(automobileData.automobile);
            setCustomers(customerData.customer);
            setTechnicians(technicianData.technician);

        }
    }
    useEffect(() => {
        fetchData();
    }, []

    );

}
