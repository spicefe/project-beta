import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import CustomerList from './CustomerList';
import SaleList from './SaleList';
import SaleForm from './SaleForm';
import SalespersonHistory from './SalespersonHistory';
import AutomobileList from './AutomobileList';
import ManufacturerList from './ManufacturerList';
import VehicleList from './VehicleModelList';
import ManufacturerForm from './ManufacturerForm';
import VehicleForm from './VehicleModelForm';
import AutomobileForm from './AutomobileForm';
import TechnicianForm from './TechnicianForm';
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salespeople">
            <Route path="" element={<SalespeopleList />} />
            <Route path="/salespeople/new" element={<SalespersonForm />} />
            <Route path="/salespeople/history" element={<SalespersonHistory />} />
          </Route>
          <Route path="/customers">
            <Route path="" element={<CustomerList  />} />
            <Route path="/customers/new" element={<CustomerForm />} />
          </Route>
          <Route path="/sales">
            <Route path="" element={<SaleList  />} />
            <Route path="/sales/new" element={<SaleForm />} />
          </Route>
          <Route path="/automobiles">
            <Route path="" element={<AutomobileList  />} />
            <Route path="/automobiles/new" element={<AutomobileForm />} />
          </Route>
          <Route path="/models">
            <Route path="" element={<VehicleList />} />
            <Route path="/models/new" element={<VehicleForm />} />
          </Route>
          <Route path="/manufacturers">
            <Route path="" element={<ManufacturerList />} />
            <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
